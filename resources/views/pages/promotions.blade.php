@extends('layouts.master')

@section('NoiDung')

<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
   <!-- section -->
   <div class="section padding_layout_1 gross_layout">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Chương trình khuyến mãi tháng 12/2020</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md-12 white margin_bottom_15">
              <div class="full">
                <div class="service_news_inner">
                  <div class="icon text_align_left"><img src="images/it_service/promotions1.jpg" alt="#" style="width: 100%;"/></div>
                  <h3 class="service-heading">Những người bạn chơi cùng nhau, hãy cùng nhau lưu lại - hãy kiểm tra chương trình mang lại bạn bè của chúng tôi!</h3>
                  <p class="large">Chỉ là một lý do để đi ra ngoài! Mang theo một người bạn và cả hai bạn sẽ nhận được 5$ giảm giá dịch vụ của mình! Yêu cầu mua tối thiểu 50$.</p>
                </div>
              </div>
            </div>
            <div class="col-md-12 white margin_bottom_15">
              <div class="full">
                <div class="service_news_inner">
                  <div class="icon text_align_left"><img src="images/it_service/promotions2.jpg" alt="#" style="width: 100%;"/></div>
                  <h3 class="service-heading">GIẢM 10% chỉ dịch vụ từ $ 50 trở lên.</h3>
                  <p class="large">Một phiếu giảm giá cho mỗi khách hàng. Không phù hợp với đề nghị nào cả. Cung cấp hợp lệ cho dịch vụ có giá cao nhất.</p>
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
          <div class="side_bar">
            <div class="card introduce side_bar_news">
                <div class="card-header  white text_align">
                    <h1>Chat với cửa hàng</h1>
                </div>
                <div class="card-content collapse show white">
                    <div class="card-body">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=messages&width=325px&height=340px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="340px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    </div>
                </div>
              </div>
            <div class="card introduce">
              <div class="card-header  white">
              </div>
              <div class="card-content collapse show white">
                  <div class="card-body">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=timeline&width=325px&height=500px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->


@endsection