1. Cài bản mới nhất của php
2. Clone code + update composer 2.x -> composer install
3. Copy .env.example -> .env
4. Cài key laravel
5. Tạo database theo tên của dự án
6. Add laravel.sql vào database vừa tạo
7. Kết nối database bằng file .env -> database_name
8. Tạo project trên git cá nhân và đẩy thư mục lên để triển khai dự án

Cách đẩy project lên gitlab: https://www.youtube.com/watch?v=LgeQR3sFiRI&ab_channel=OnesinusSPT
