@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 0px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main_heading text_align_center">
          <h2><Label>Dịch vụ của chúng tôi</Label></h2>
        </div>
      </div>
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Dòng nails cao cấp</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('mong-tay') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Sơn Móng Hàn Xẻn</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Mẫu Nail Cho Cô Dâu</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa xuân</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Lá Cây</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 0px;">
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Thời trang</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('mong-tay') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/5.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa Hè</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa Hè</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/7.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/8.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">NKiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Sơn Móng Hàn Xẻn</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Mẫu Nail Cho Cô Dâu</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa xuân</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Lá Cây</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 60px;">
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Chăm sóc móng</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('mong-tay') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/5.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa Hè</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <a href="#" data-toggle="modal" data-target="#service_bar">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4>Kiểu Nail Mùa Hè</h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
              </a>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/7.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price"> $12.49</span></p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
            <div class="product_list">
              <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/8.jpg" alt=""> </div>
              <div class="product_detail_btm">
                <div class="center">
                  <h4><a href="#" data-toggle="modal" data-target="#service_bar">NKiểu Nail Mùa Hè</a></h4>
                </div>
                <div class="product_price">
                  <p><span class="new_price">$25.00</span></p>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- end section -->
@endsection