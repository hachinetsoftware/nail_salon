@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="side_bar_news">
            <h4>Search</h4>
            <div class="side_bar_search">
              <div class="input-group stylish-input-group">
                <input class="form-control" placeholder="Search" type="text">
              </div>
              <div class="search_bt">
                <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end inner page banner -->
<div class="section padding_layout_1">
  <div class="container">
    <div class="row">
      <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-xs-12"></div>
      <div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-xs-12">
        <div class="row">
          <div class="full">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="main_heading text_align_center">
                <h2><Label>LIÊN LẠC</Label></h2>
                <p class="large">Đánh giá và gửi ý kiến phản hồi cho chúng tôi để cùng hợp tác và phát triển.</p>
              </div>
            </div>
            <div class="contact_information">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 adress_cont">
                <div class="information_bottom text_align_center">
                  <div class="icon_bottom"> <i class="fa fa-road" aria-hidden="true"></i>
                  <p>Địa chỉ</p> 
                  </div>
                  <div class="info_cont">
                    <h4>87 Nguyễn Thị Minh Khai,Hưng Bình..</h4>
                    <p>Vinh, Nghệ An</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 adress_cont">
                <div class="information_bottom text_align_center">
                  <div class="icon_bottom"> <i class="fa fa-user" aria-hidden="true"></i> 
                    <p>Số điện Thoại</p>
                  </div>
                  <div class="info_cont">
                    <h4>0011 234 56789</h4>
                    <p>Thứ Hai-Thứ Sáu 8:30am-6:30pm</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 adress_cont">
                <div class="information_bottom text_align_center">
                  <div class="icon_bottom"> <i class="fa fa-envelope" aria-hidden="true"></i> 
                  <p>Email</p>
                  </div>
                  <div class="info_cont">
                    <h4>nailssalon@gmail.com</h4>
                    <p>Hoạt đọng 24/7</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 contant_form">
              <h2 class="text_align_center">GỬI TIN NHẮN</h2>
              <div class="form_section">
                <form class="form_contant" action={{ url('/') }}>
                  <fieldset>
                  <div class="row">
                    <div class="field col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <input class="field_custom" placeholder="Họ tên" type="text">
                    </div>
                    <div class="field col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <input class="field_custom" placeholder="Số điện thoại" type="text">
                    </div>
                    <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <input class="field_custom" placeholder="Email" type="email">
                    </div>
                    <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <textarea class="field_custom" placeholder="Tin nhắn"></textarea>
                    </div>
                    <div class="center"><a class="btn main_bt" href="#">Gửi ngay</a></div>
                  </div>
                  </fieldset>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
  <!-- map-->
  <div class="map">
    <div ><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d944.9414314631845!2d105.6778782291951!3d18.674505969707038!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139cf1dc902fdeb%3A0x70689ca8f2e72ed0!2sHachinet%20Vinh!5e0!3m2!1svi!2s!4v1606097767718!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
  </div>
  <!-- map-->
  @endsection