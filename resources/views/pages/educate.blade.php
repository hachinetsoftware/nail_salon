@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 0px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main_heading text_align_center">
          <h2><Label>Dạy nails</Label></h2>
        </div>
      </div>
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Cho người mới</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('dao-tao-moi') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/1.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Hướng Dẫn Vẽ Nail Aztec Chỉ Trong 5 Phút</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Vẽ nail Aztec chỉ trong 5 phút? Đây không phải là chuyện đùa. Và bạn không cần phải là một chuyên gia vẽ móng cũng có thể thực hiện được</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/2.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Vẽ Nail Với Cảm Hứng Từ Kỳ Nghỉ Hè Ở Miền Nhiệt Đới</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Mùa hè đang thật sự hiện hữu! Bạn đã có kế hoạch gì cho kỳ nghỉ hè năm nay? Một chuyến du lịch đến các miền nhiệt đới? </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/4.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Nghệ Thuật Vẽ Nail Cho Người Mới: Hoa Tím</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Mẫu nail tím mà chúng tôi giới thiệu dưới đây là mẫu thiết kế hoàn hảo cho người mới làm </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 0px;">
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Dạy nails cơ bản</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('dao-tao-moi') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/1.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Bộ Dụng Cụ Làm Nail Từ Cơ Bản Đến Chuyên Nghiệp Gồm Những Gì? </h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Nail là một trong những nghề được nhiều người theo học nhất hiện nay, đặc biệt là các bạn trẻ. Tuy nhiên </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/2.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Những Mẫu Nail Họa Tiết Floral Siêu Dễ Thương</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Hãy cùng khám phá những mẫu nail họa tiết floral đơn giản nhưng vô cùng ấn tượng nhé!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Thủ Thuật Dùng Giấy Foil Vào Mẫu Nail: Bữa Tiệc Màu Sắc </h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Bạn đã bao giờ thử dùng giấy foil cho mẫu thiết kế móng của mình chưa? Nếu chưa, bạn đã bỏ lỡ một thủ thuật thú vị rồi đấy!</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/4.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Cách Sử Dụng Sticker Dán Móng Thật Chuyên Nghiệp: 8 Mẹo Hay Nhất </h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Sticker dán móng là một lựa chọn tuyệt vời nếu bạn muốn thay đổi phong cách cho bộ móng yêu của mình</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 60px;">
    <div class="container white">
        <div class="educate_header">
            <div class="left">
            <h3><i class="fa fa-graduation-cap mr-1"></i>Dạy nails nâng cao</h3>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('dao-tao-moi') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/1.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Bộ Dụng Cụ Làm Nail Từ Cơ Bản Đến Chuyên Nghiệp Gồm Những Gì?</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Nail là một trong những nghề được nhiều người theo học nhất hiện nay, đặc biệt là các bạn trẻ</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/2.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Nghệ Thuật Vẽ Móng 3D Lấy Cảm Hứng Từ Thập Niên 80</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Tạo hình cầu vồng bằng kỹ thuật vẽ móng 3D, tại sao không? Đây là một thử thách vô cùng thú vị </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Mẫu Móng Hoa Nhẹ Nhàng Cho Cô Dâu</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Bạn sẽ chọn mẫu móng nào cho ngày lễ quan trọng nhất của mình? Hãy thử mẫu hoa dưới đây nếu tone</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/4.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Hướng Dẫn Vẽ Nail: Mẫu Móng Vân Đá Phong Cách Dạ Hội Hoàng Gia</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Mẫu móng vân đá đến từ nghệ sĩ nail Jeannette Alfaro đã tạo nên một sự lựa chọn hoàn hảo cho những dịp đặc biệt.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
@endsection