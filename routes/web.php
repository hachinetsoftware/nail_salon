<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function()  {
    return view('pages/home');
});
// về nailssalon
Route::get('ve-nails-salon', function()  {
    return view('pages/about');
});

Route::get('shop', function()  {
    return view('pages/shop');
});

// xu hướng
Route::get('xu-huong', function()  {
    return view('pages/news');
});
// chi tiết
Route::get('chi-tiet', function()  {
    return view('pages/news_detail');
});
// tin tức
Route::get('tin-tuc', function()  {
    return view('pages/news_service');
});
// liên hệ
Route::get('lien-he', function()  {
    return view('pages/contact');
});
// Đặt lịch
Route::get('dat-lich-hen', function()  {
    return view('pages/booking');
});
// Dịch vụ 
Route::get('dich-vu', function()  {
    return view('pages/service');
});

Route::get('mong-tay', function()  {
    return view('pages/service_item');
});
// đào tạo
Route::get('dao-tao', function()  {
    return view('pages/educate');
});

Route::get('dao-tao-moi', function()  {
    return view('pages/educate_service');
});

Route::get('dao-tao-chi-tiet', function()  {
    return view('pages/educate_detai');
});
// khuyến mãi
Route::get('khuyen-mai', function()  {
    return view('pages/promotions');
});

