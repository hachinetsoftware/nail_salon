@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
<div class="section padding_layout_1" style="padding-top: 48px;padding-bottom: 0px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="main_heading text_align_center">
          <h2><Label>Dạy nails</Label></h2>
        </div>
      </div>
    <div class="container white">
        <div class="educate_header">
            <div class="left">
              <div class="dropdown">
                <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Dropdown link
                </a>
              
                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </div>
            <div class="pull-right">
              <a class="btn main_bt" href={{ url('dao-tao-moi') }}>Xem thêm</a>
            </div>
            </div>
          </div>
      <div class="row educate_content">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/1.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Hướng Dẫn Vẽ Nail Aztec Chỉ Trong 5 Phút</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Vẽ nail Aztec chỉ trong 5 phút? Đây không phải là chuyện đùa. Và bạn không cần phải là một chuyên gia vẽ móng cũng có thể thực hiện được</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/2.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Vẽ Nail Với Cảm Hứng Từ Kỳ Nghỉ Hè Ở Miền Nhiệt Đới</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Mùa hè đang thật sự hiện hữu! Bạn đã có kế hoạch gì cho kỳ nghỉ hè năm nay? Một chuyến du lịch đến các miền nhiệt đới? </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
          <div class="full text_align_left product_list  educate_item">
            <div class="center">
              <div class="product_img service_img"> <img src="images/it_service/4.jpg" alt="#" /> </div>
            </div>
            <div class="col-md-12">
                <h4 class="theme_color el-2">Nghệ Thuật Vẽ Nail Cho Người Mới: Hoa Tím</h4>
                <p>
                    <i class="fa fa-calendar"></i> 
                    <span class="ml-1">20/11/2020</span>
                </p>
                <p class="el-2">Mẫu nail tím mà chúng tôi giới thiệu dưới đây là mẫu thiết kế hoàn hảo cho người mới làm </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

@endsection