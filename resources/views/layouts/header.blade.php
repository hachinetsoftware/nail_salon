<!-- header -->
<header id="default_header" class="header_style_1">
	<!-- header top -->
	<div class="header_top">
	  <div class="container">
		<div class="row">
		  <div class="col-md-8">
			<div class="full">
			  <div class="topbar-left">
				<ul class="list-inline">
				  <li> <span data-toggle="tooltip"  data-placement="bottom" title ="Địa Chỉ" class="topbar-label"><i class="fa fa-map-marker mr-1"></i></span> <span class="topbar-hightlight">87 Nguyễn Thị Minh Khai,TP Vinh</span> </li>
				  <li> <span data-toggle="tooltip"  data-placement="bottom" title ="Email" class="topbar-label"><i class="fa fa-envelope-o"></i></span> <span class="topbar-hightlight"><a href="mailto:info@yourdomain.com">contact@hachinet.com</a></span> </li>
				</ul>
			  </div>
			</div>
		  </div>
		  <div class="col-md-4 right_section_header_top">
			<div class="float-right">
			  <div class="social_icon">
				<ul class="list-inline icon_rep">
				  <li class="social-icon fb"><a class="fa fa-facebook" href="https://www.facebook.com/" title="Facebook" aria-hidden="true" target="_blank"></a></li>
				  <li class="social-icon gp"><a class="fa fa-google-plus" href="https://plus.google.com/" title="Google+" aria-hidden="true" target="_blank"></a></li>
				  <li class="social-icon tw"><a class="fa fa-twitter" href="https://twitter.com" title="Twitter" aria-hidden="true" target="_blank"></a></li>
				  <li class="social-icon ln"><a class="fa fa-linkedin" href="https://www.linkedin.com" title="LinkedIn" aria-hidden="true" target="_blank"></a></li>
				  <li class="social-icon in"><a class="fa fa-instagram" href="https://www.instagram.com" title="Instagram" aria-hidden="true" target="_blank"></a></li>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- end header top -->
	<!-- header bottom -->
	<div class="header_bottom">
	  <div class="container">
		<div class="row">
		  <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
			<!-- logo start -->
			<div class="logo"> <a href={{ url('/') }}><img src="images/logos/hachinet_logo.png" alt="logo" /></a> </div>
			<!-- logo end -->
		  </div>
		  <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
			<!-- menu start -->
			<div class="menu_side">
			  <div id="navbar_menu">
				<ul class="first-ul">
				  <li>
					<a class="{{ '/' == request()->path() ? 'active' : '' }}" href={{ url('/') }}> 
						<div class="nav-icon"><i class="fa fa-home" style="font-size: 25px;"></i></div>
						<div class="nav-title">Trang chủ</div>
					</a>
				  </li>
				  <li>
					  <a class="{{ 've-nails-salon' == request()->path() ? 'active' : '' }}" href={{ url('ve-nails-salon') }}>
						<div class="nav-icon"><i class="fa fa-id-card"></i></div>
						<div class="nav-title">NailsSalon</div>
					  </a></li>
				  <li> 
					  <a class="{{ 'dich-vu' == request()->path() ? 'active' : '' }}" href={{ url('dich-vu') }}>
						<div class="nav-icon"><i class="fa fa-briefcase"></i></div>
						<div class="nav-title"> Dịch vụ</div>
					  </a>
				  </li>
				  <li> 
					<a class="{{ 'khuyen-mai' == request()->path() ? 'active' : '' }}" href={{ url('khuyen-mai') }}>
					  <div class="nav-icon"><i class="fa fa-gift" style="font-size: 25px;"></i></div>
					  <div class="nav-title">Khuyến mãi</div>
					</a>
				</li>
				  <li> 
					  <a class="{{ 'xu-huong' == request()->path() ? 'active' : '' }}" href={{ url('xu-huong') }}>
						<div class="nav-icon"><i class="fa fa-file-text"></i></div>
						<div class="nav-title">Xu Hướng</div>
					  </a>
				  </li>
				  <li> 
					  <a class="{{ 'dao-tao' == request()->path() ? 'active' : '' }}" href={{ url('dao-tao') }}>
						<div class="nav-icon"><i class="fa fa-graduation-cap" style="font-size: 25px;"></i></div>
						<div class="nav-title">Đào Tạo</div> 
					  </a>
				  <li> 
					  <a class="{{ 'lien-he' == request()->path() ? 'active' : '' }}" href={{ url('lien-he') }}>
						<div class="nav-icon"><i class="fa fa-phone"></i></div>
						<div class="nav-title">Liên hệ</div> 
					  </a>
				  </li>
				</ul>
			  </div>
			  <div class="search_icon">
				<ul>
				  <li><span data-toggle="tooltip"  data-placement="bottom" title ="Tìm kiếm"><a href="#" data-toggle="modal" data-target="#search_bar"><i class="fa fa-search" aria-hidden="true"></i></a></span></li>
				</ul>
			  </div>
			</div>
			<!-- menu end -->
		  </div>
		</div>
	  </div>
	</div>
	<!-- header bottom end -->
  </header>
    <!-- Modal -->
	<div class="modal fade" id="search_bar" role="dialog">
		<div class="modal-dialog" >
		  <!-- Modal content-->
		  <div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
			</div>
			<div class="modal-body">
			  <div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 offset-lg-2 offset-md-2 offset-sm-2 col-xs-10 col-xs-offset-1">
				  <div class="navbar-search">
					<form action="#" method="get" id="search-global-form" class="search-global">
					  <input type="text" placeholder="Nhập để tìm kiếm" autocomplete="off" name="s" id="search" value="" class="search-global__input">
					  <button class="search-global__btn"><i class="fa fa-search"></i></button>
					</form>
				  </div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	  <!-- End Model search bar -->
  <!-- end header -->