@extends('layouts.master')

@section('NoiDung')

<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
  <div class="section padding_layout_1 news_grid">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
          <div class="full">
            <div class="news_section margin_bottom_0">
              <div class="news_feature_img"> <img class="img-responsive" src="images/it_service/post-06.jpg" alt="#"> </div>
              <div class="news_feature_cantant">
                <p class="news_head">Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</p>
                <div class="post_info">
                  <ul>
                    <li><i class="fa fa-user" aria-hidden="true"></i> Lượt xem</li>
                    <li><i class="fa fa-comment" aria-hidden="true"></i> 5</li>
                    <li><i class="fa fa-calendar" aria-hidden="true"></i> 12 Aug 2017</li>
                  </ul>
                </div>
                <p>Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ. </p>
                <p>40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California. Trước chuyến đi, Hedren đã băn khoăn suy nghĩ về việc đào tạo một kỹ năng hay ngành nghề nào đó để giúp họ tự nuôi sống bản thân ở nước Mỹ.

                    Khi gặp một nhóm phụ nữ người Việt, bà ngạc nhiên nhận thấy họ rất say mê móng tay của mình.</p>
              </div>
              <div class="full testimonial_simple_say margin_bottom_30_all" style="margin-top:0;">
                <div class="qoute_testimonial"><i class="fa fa-quote-left" aria-hidden="true"></i></div>
                <p class="margin_bottom_0"><i>Chúng tôi đang cố gắng định hướng nghề nghiệp cho họ. Tôi đưa đến một người thợ may và một người đánh máy, bất kỳ cách nào để họ học được nghề gì đó. Và họ thích những chiếc móng tay của tôi.</i></p>
                <p class="large_2 theme_color"> Hedren, ngôi sao hiện đã 85 tuổi, kể.</p>
              </div>
              <p>Hedren thành lập viện thẩm mỹ riêng và mở một lớp học địa phương để dạy cho 20 phụ nữ cách làm móng tay. Nhiều người trong số đó sau này định cư ở Nam California và cung cấp dịch vụ chăm sóc móng với mức giá thấp hơn các đối thủ cạnh tranh. Điều này đã làm thay đổi nhanh chóng và mạnh mẽ bộ mặt của ngành công nghiệp làm móng trong khu vực. <br>
                <br>
                Mỗi bộ móng ở các mỹ viện sang trọng có giá 50 USD, nhưng ở salon của người Mỹ gốc Việt có giá thấp hơn 30-50%, theo tạp chí Nails. Hiện nay, ngành công nghiệp nail trị giá 8 tỷ USD và 80% thợ làm móng ở Nam California, 51% trên cả nước Mỹ, là người Việt. Nhiều người trong các thợ làm móng đó là "hậu duệ" của 20 phụ nữ mà Hedren gặp từ thuở ban đầu. <br>
                <br>
                Nghề nail không chỉ giúp nhiều người có cuộc sống ổn định ở Mỹ, với thu nhập khoảng 645 USD một tuần theo số liệu năm 2014, mà còn tạo điều kiện để họ gửi tiền về cho gia đình ở Việt Nam. Le nhớ lại khi mới bắt đầu vào nghề, cô cố gắng gửi 50-100 USD về nhà hàng tháng, dù tiền lương chỉ vừa đủ để cô nuôi sống bản thân mình.</p>
              <div class="bottom_info margin_bottom_30_all">
                <div class="pull-right">
                  <div class="shr">Share: </div>
                  <div class="social_icon">
                    <ul>
                      <li class="fb"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                      <li class="twi"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                      <li class="gp"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                      <li class="pint"><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="comment_section">
                <div class="pull-left text_align_left">
                  <div class="full">
                    <div class="preview_commt"> <a class="comment_cantrol preview_commat" href="it_news_detail.html"> <img class="img-responsive" src="images/it_service/post-04.jpg" alt="#"> <span><i class="fa fa-angle-left"></i> Previous</span> </a> </div>
                  </div>
                </div>
                <div class="pull-right text_align_right">
                  <div class="full">
                    <div class="next_commt"> <a class="comment_cantrol preview_commat" href="it_news_detail.html"> <img class="img-responsive" src="images/it_service/post-03.jpg" alt="#"> <span>Next <i class="fa fa-angle-right"></i></span> </a> </div>
                  </div>
                </div>
              </div>
              <div class="post_commt_form">
                <iframe name="f29195a16aaa28" width="550px" height="100px" data-testid="fb:comments Facebook Social Plugin" title="fb:comments Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v5.0/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1fe5789dfa6558%26domain%3Dnetbee.vn%26origin%3Dhttps%253A%252F%252Fnetbee.vn%252Ff2fbf095e80eba8%26relation%3Dparent.parent&amp;container_width=1112&amp;height=100&amp;href=https%3A%2F%2Fnetbee.vn%2Ftin-tuc%2F150%2Fho-tro-cua-chinh-phu-uc-doi-voi-du-hoc-sinh-trong-thoi-diem-dich-covid-19&amp;locale=en_US&amp;numposts=5&amp;sdk=joey&amp;version=v5.0&amp;width=550" style="border: none; visibility: visible; width: 550px; height: 262px;" class=""></iframe>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
            <div class="side_bar">
              <div class="side_bar_news">
                <h4>Bài đăng gần đây</h4>
                <div class="recent_post">
                  <ul>
                    <li>
                      <p class="post_head"><a href={{ url('chi-tiet') }}>Lí giải vì sao người Việt làm nails nhiều đến như vậy trên đất Mỹ</a></p>
                      <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                    </li>
                    <li>
                      <p class="post_head"><a href={{ url('chi-tiet') }}>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</a></p>
                      <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                    </li>
                    <li>
                      <p class="post_head"><a href={{ url('chi-tiet') }}>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</a></p>
                      <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="side_bar_news">
                <h4>Danh mục</h4>
                <div class="categary">
                  <ul>
                    <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i>Dòng nails cao cấp</a></li>
                    <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i> Thời trang</a></li>
                    <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i> Chăm sóc móng</a></li>
                  </ul>
                </div>
              </div>
              <div class="card introduce">
                <div class="card-header">
                </div>
                <div class="card-content collapse show">
                    <div class="card-body">
                      <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                    </div>
                </div>
            </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  
@endsection