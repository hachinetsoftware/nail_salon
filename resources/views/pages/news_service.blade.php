@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
  <div class="section padding_layout_2 news_list">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right white">
          <div class="full news_content">
            <div class="news_header">
                <div class="left" style="padding: 0 3.5rem 0;">
                <h3 style="margin-top: 0;"><i class="fa fa-newspaper-o"></i>Chăm sóc móng</h3>
                </div>
            </div>
            <div class="news_content">
                <div class="col-lg-12 pr-3 pl-3 py-3 product_list" style="top: -13px;">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-06.jpg" alt="#"> </div>
                  <div class="news_feature_cantant flex-column col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-3">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                    <div class="pull-right">
                      <a class="btn main_bt" href={{ url('chi-tiet') }}>Xem thêm</a>
                    </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3 product_list">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-04.jpg" alt="#"> </div>
                  <div class="news_feature_cantant col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</h4>
                  </a>
                    <span>20/11/2020</span>
                    <p class="el-3">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                    <div class="pull-right">
                      <a class="btn main_bt" href={{ url('chi-tiet') }}>Xem thêm</a>
                    </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3 product_list">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-03.jpg" alt="#"> </div>
                  <div class="news_feature_cantant col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Lí giải vì sao người Việt làm nails nhiều đến như vậy trên đất Mỹ?</h4>
                  </a>
                    <span>20/11/2020</span>
                    <p class="el-3">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                    <div class="pull-right">
                      <a class="btn main_bt" href={{ url('chi-tiet') }}>Xem thêm</a>
                    </div>
                  </div>
                  </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3 product_list">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-06.jpg" alt="#"> </div>
                  <div class="news_feature_cantant col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Lí giải vì sao người Việt làm nails nhiều đến như vậy trên đất Mỹ?</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-3">Một số phụ nữ gốc Á có thể bỏ túi 100 đến 150 USD mỗi ngày từ nghề "làm nail", tuy nhiên, một số người khác phải nhận mức lương rất thấp vì sự phân biệt đối xử của chủ tiệm. ....  </p>
                    <div class="pull-right">
                      <a class="btn main_bt" href={{ url('chi-tiet') }}>Xem thêm</a>
                    </div>
                  </div>
                </div>
                </div>
                <div class="col-lg-12 mt-2 mb-2 pr-3 pl-3 pt-3 product_list">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-03.jpg" alt="#"> </div>
                  <div class="news_feature_cantant col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-3">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                    <div class="pull-right">
                      <a class="btn main_bt" href={{ url('chi-tiet') }}>Xem thêm</a>
                    </div>
                  </div>
                </div>
                </div>
            </div>
            <div class="center" style="margin-top: 30px;">
              <ul class="pagination style_1">
                <li><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="it_news_grid.html">2</a></li>
                <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
            <div class="side_bar">
                <div class="side_bar_news white">
                    <h4><i class="fa fa-newspaper-o mr-1"></i>Bài đăng gần đây</h4>
                    <div class="recent_post">
                      <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                        <div class="d-flex">
                        <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-03.jpg" alt="#"> </div>
                        <div class="news_feature_cantant flex-column col-sm-7">
                          <a href={{ url('chi-tiet') }}>
                          <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
                          </a>
                          <span>20/11/2020</span>
                          <p class="el-2">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                        </div>
                      </div>
                      </div>
                      <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                        <div class="d-flex">
                        <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-04.jpg" alt="#"> </div>
                        <div class="news_feature_cantant flex-column col-sm-7">
                          <a href={{ url('chi-tiet') }}>
                          <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
                          </a>
                          <span>20/11/2020</span>
                          <p class="el-2">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                        </div>
                      </div>
                      </div>
                      <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                        <div class="d-flex">
                        <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/post-06.jpg" alt="#"> </div>
                        <div class="news_feature_cantant flex-column col-sm-7">
                          <a href={{ url('chi-tiet') }}>
                          <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
                          </a>
                          <span>20/11/2020</span>
                          <p class="el-2">Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ.40 năm trước, nữ diễn viên Hollywood đến thăm Làng Hy Vọng của người Việt gần Sacramento, California.  </p>
                        </div>
                      </div>
                      </div>
                    </div>
                </div>
                <div class="side_bar_news white">
                    <h4><i class="fa fa-newspaper-o mr-1"></i>Danh mục</h4>
                    <div class="categary">
                    <ul>
                        <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i> Dòng nails cao cấp</a></li>
                        <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i> Thời trang</a></li>
                        <li><a href={{ url('tin-tuc') }}><i class="fa fa-caret-right"></i> Chăm sóc móng</a></li>
                    </ul>
                    </div>
                </div>
                <div class="card introduce">
                    <div class="card-header  white">
                    </div>
                    <div class="card-content collapse show white">
                        <div class="card-body">
                            <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=timeline&width=325px&height=500px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                    
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  

  @endsection