@extends('layouts.master')

@section('NoiDung')

<!-- section -->
<div id="slider" class="section main_slider">
    <div class="container-fuild">
      <div class="row">
        <div class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
          <!-- START REVOLUTION SLIDER 5.0.7 auto mode -->
          <div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>
              <li data-index="rs-1812" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="images/it_service/slide1.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="CHÀO MỪNG ĐẾN VỚI NAIL SALON" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/it_service/slide1.jpg"  alt="#"  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"  class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. BG -->
                <!-- LAYER NR. 2 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-112" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                data-fontsize="['70','70','70','35']"
                                data-lineheight="['70','70','70','50']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1000" 
                                data-splitin="chars" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                data-elementdelay="0.05" 
                                style="z-index: 6; white-space: nowrap;">CHÀO MỪNG ĐẾN VỚI NAIL SALON </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-412" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1500" 
                                data-splitin="none" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                style="z-index: 7; white-space: nowrap;">NƠI TỐT NHẤT ĐỂ LÀM MÓNG TAY , MÓNG CHÂN </div>
              </li>
              <li data-index="rs-181" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="images/it_service/slide2.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="CẬP NHẬP CÁC MẪU MỚI NHẤT" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/it_service/slide2.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"  class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. 2 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-11" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                data-fontsize="['70','70','70','35']"
                                data-lineheight="['70','70','70','50']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1000" 
                                data-splitin="chars" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                data-elementdelay="0.05" 
                                style="z-index: 6; white-space: nowrap;">CẬP NHẬP CÁC MẪU MỚI NHẤT </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-41" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1500" 
                                data-splitin="none" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                style="z-index: 7; white-space: nowrap;"> Đủ mọi kiểu dáng </div>
              </li>
              <li data-index="rs-18" data-transition="zoomin" data-slotamount="7"  data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000"  data-thumb="images/it_service/slide3.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="KHÔNG GIAN SANG TRỌNG" data-description="">
                <!-- MAIN IMAGE -->
                <img src="images/it_service/slide3.jpg"  alt=""  data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0"  class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <!-- LAYER NR. BG -->
               
                <!-- LAYER NR. 2 -->
                <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-1" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                data-fontsize="['70','70','70','35']"
                                data-lineheight="['70','70','70','50']"
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1000" 
                                data-splitin="chars" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                data-elementdelay="0.05" 
                                style="z-index: 6; white-space: nowrap;">KHÔNG GIAN SANG TRỌNG </div>
                <!-- LAYER NR. 3 -->
                <div class="tp-caption NotGeneric-SubTitle   tp-resizeme rs-parallaxlevel-0" 
                                id="slide-18-layer-4" 
                                data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                data-y="['middle','middle','middle','middle']" data-voffset="['52','51','51','31']" 
                                data-width="none"
                                data-height="none"
                                data-whitespace="nowrap"
                                data-transform_idle="o:1;"
                                data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                                data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                                data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;" 
                                data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;" 
                                data-start="1500" 
                                data-splitin="none" 
                                data-splitout="none" 
                                data-responsive_offset="on" 
                                style="z-index: 7; white-space: nowrap;">Giàn chuyên gia nhiều năm kinh nghiệm </div>
              </li>
            </ul>
            <div class="tp-static-layers"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 main_color wow fadeInUpBig" data-wow-delay="1s" style="padding-bottom: 0">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Sứ Mệnh Của Chúng Tôi Là Làm Hài Lòng Khách Hàng</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 about_cont_news" style="padding: 84px 15px;">
          <div class="full text_align">
            <p>Newport Nails không chỉ là lấp lánh và đánh bóng. Chúng tôi cung cấp cho tất cả các khách hàng của mình một lối thoát khỏi những căng thẳng của cuộc sống hàng ngày. Cho dù bạn đang yêu cầu làm móng tay hay chăm sóc da mặt, Newport Nails đều có môi trường thư giãn mà bạn cần. Ưu tiên hàng đầu của chúng tôi là khách hàng của chúng tôi, đó là lý do tại sao bạn sẽ được chăm sóc tận tình ngay từ khi bạn đến.</p><br>
            <p>Tiệm của chúng tôi cung cấp vô số dịch vụ chăm sóc móng, chăm sóc da, tẩy lông và chăm sóc da mặt, đảm bảo bạn sẽ cảm thấy sảng khoái và trẻ hóa.</p>
          </div>
        </div>
        <div class="col-lg-6 col-md-11 col-sm-12 about_feature_img">
          <div class="full text_align_center"> <img class="img-responsive img-right" src="images/it_service/newportnails.jpg" alt="#"> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
<div class="section bg_section padding_layout_1 light_silver wow flipInX" data-wow-delay="2s" style="padding-top: 48px;padding-bottom: 108px;">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div>
          <div class="main_heading text_align_center">
            <h2>TẠI SAO LẠI CHỌN CHÚNG TÔI</h2>
            <p class="large">Dịch vụ là móng đẹp nhất với giá rẻ nhất!</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 icon_box">
        <div class="full text_align_center product_list">
          <div class="center">
            <div class="icon"> <img src="images/it_service/i1.png" alt="#" /> </div>
          </div>
          <h4 class="theme_color">Sự hài lòng của khách hàng</h4>
          <p>Dịch vụ được 95% khách hàng đánh giá tích cực!</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 icon_box">
        <div class="full text_align_center product_list">
          <div class="center">
            <div class="icon"> <img src="images/it_service/i2.png" alt="#" /> </div>
          </div>
          <h4 class="theme_color">Nhân viên thân thiện</h4>
          <p>Đội ngũ nhân viên thân thiện luôn đối sử tốt với khách hàng!</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 icon_box">
        <div class="full text_align_center product_list">
          <div class="center">
            <div class="icon"> <img src="images/it_service/i3.png" alt="#" /> </div>
          </div>
          <h4 class="theme_color">Khuyến mãi</h4>
          <p>Có nhiều chương trình giảm giá cực sốc!</p>
        </div>
      </div>
      <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 icon_box">
        <div class="full text_align_center product_list">
          <div class="center">
            <div class="icon"> <img src="images/it_service/i4.png" alt="#" /> </div>
          </div>
          <h4 class="theme_color">Đặt lịch online</h4>
          <p>Dịch vụ đặt lịch online nhanh chóng tiện lợi!</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 gross_layout main_color wow bounceInRight">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Dịch vụ mà chúng tôi cung cấp</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="full">
                <a href={{ url('mong-tay') }}>
                <div class="service_news_inner">
                  <div class="icon text_align_left"><img src="images/it_service/si1.jpg" alt="#" style="width: 100%;"/></div>
                  <h3 class="service-heading">Móng tay</h3>
                  <p class="large">Tạo kiểu móng tay thời thượng đẹp mắt.</p>
                </div>
              </a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="full">
                <a href={{ url('mong-tay') }}>
                <div class="service_news_inner">
                  <div class="icon text_align_left"><img src="images/it_service/si2.jpg" alt="#" style="width: 100%;"/></div>
                  <h3 class="service-heading">Móng chân</h3>
                  <p class="large">Tạo kiểu móng chân thời thượng đẹp mắt.</p>
                </div>
              </a>
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 padding_top_0 wow bounceInUp" >
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Sản phẩm của chúng tôi</h2>
              <p class="large">Chúng tôi luôn tạo ra các sản phảm theo yêu cầu người dùng.</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Kiểu Sơn Móng Hàn Xẻn</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
          </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Mẫu Nail Cho Cô Dâu</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Kiểu Nail Mùa xuân</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Kiểu Nail Lá Cây</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/5.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Kiểu Nail Mùa Hè</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <a href="#" data-toggle="modal" data-target="#service_bar">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4>Kiểu Nail Mùa Hè</h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
            </a>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/7.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/8.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">NKiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price"> $12.49</span></p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 margin_bottom_30_all">
          <div class="product_list">
            <div class="product_img service_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
            <div class="product_detail_btm">
              <div class="center">
                <h4><a href="#" data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
              </div>
              <div class="product_price">
                <p><span class="new_price">$25.00</span></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 bg_section wow fadeInUp">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Tin Tức Mới Nhất</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-4 col-md-6 pb-3">
          <a href={{ url('chi-tiet') }}>
          <div class="full news_colum product_list">
            <div class="news_feature_img"> <img src="images/it_service/post-03.jpg" alt="#" /> </div>
            <div class="post_time">
              <p><i class="fa fa-clock-o"></i> April 16, 2018 ( In Maintenance )</p>
            </div>
            <div class="news_feature_head">
              <h4>Lí giải vì sao người Việt làm nails nhiều đến như vậy trên đất Mỹ?</h4>
            </div>
            <div class="news_feature_cont">
              <p>Nails (trang trí móng tay) phát triển mạnh mẽ tại Mỹ và có tới 40% người làm nails là người Mỹ gốc Việt. ...</p>
            </div>
          </div>
        </a>
        </div>
        <div class="col-lg-4 col-md-6 pb-3">
          <a href={{ url('chi-tiet') }}>
          <div class="full news_colum product_list">
            <div class="news_feature_img"> <img src="images/it_service/post-04.jpg" alt="#" /> </div>
            <div class="post_time">
              <p><i class="fa fa-clock-o"></i> April 16, 2018 ( In Maintenance )</p>
            </div>
            <div class="news_feature_head">
              <h4>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</h4>
            </div>
            <div class="news_feature_cont">
              <p>Một số phụ nữ gốc Á có thể bỏ túi 100 đến 150 USD mỗi ngày từ nghề "làm nail", tuy nhiên, một số người khác phải nhận mức lương rất thấp vì sự phân biệt đối xử của chủ tiệm. ...</p>
            </div>
          </div>
          </a>
        </div>
        <div class="col-lg-4 col-md-6 pb-3">
          <a href={{ url('chi-tiet') }}>
          <div class="full news_colum product_list">
            <div class="news_feature_img"> <img src="images/it_service/post-06.jpg" alt="#" /> </div>
            <div class="post_time">
              <p><i class="fa fa-clock-o"></i> April 16, 2018 ( In Maintenance )</p>
            </div>
            <div class="news_feature_head">
              <h4>Ngôi sao Mỹ giúp người Việt nổi tiếng với nghề nail</h4>
            </div>
            <div class="news_feature_cont">
              <p>Người Mỹ dễ dàng nhận ra Tippi Hedren qua vai diễn trong bộ phim kinh dị The Birds, nhưng đối với cộng đồng người Việt, bà là người đặt nền móng cho ngành công nghiệp nail làm nên tên tuổi họ...</p>
            </div>
          </div>
          </a>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 wow bounceInRight">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Theo dõi chúng tôi trên Instagram</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
<!-- LightWidget WIDGET --><script src="https://cdn.lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/92a05544f03a590b89901b0f1afbbfe7.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width:100%;border:0;overflow:hidden;"></iframe>      </div>
    </div>
  </div>
  <!-- end section -->
  <!-- section -->
  <div class="section padding_layout_1 testmonial_section white_fonts wow fadeInDown">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full" style=" margin-bottom: 76px;">
            <div class=" text_align_left">
              <h2 style="text-transform: none;">Khách hàng nói gì?</h2>
              <p class="large">Đây là những lời chứng thực của khách hàng..</p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-7">
          <div class="full">
            <div id="testimonial_slider" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ul class="carousel-indicators">
                <li data-target="#testimonial_slider" data-slide-to="0" class="active"></li>
                <li data-target="#testimonial_slider" data-slide-to="1"></li>
                <li data-target="#testimonial_slider" data-slide-to="2"></li>
              </ul>
              <!-- The slideshow -->
              <div class="carousel-inner">
                <div class="carousel-item active">
                  <div class="testimonial-container">
                    <div class="testimonial-content"> Đây là thẩm mỹ viện tốt nhất bao giờ hết! Dịch vụ thật tuyệt vời và mọi người ở đó rất thân thiện! Đó là một nơi rất sạch sẽ. Các thiết kế cho móng tay và móng chân rất đẹp! Không có nơi nào khác tương tự hoặc tốt hơn. </div>
                    <div class="testimonial-photo"> <img src="images/it_service/client1.jpg" class="img-responsive" alt="#" width="150" height="150"> </div>
                    <div class="testimonial-meta">
                      <h4>Maria Anderson</h4>
                      <span class="testimonial-position">CFO, Tech NY</span> </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="testimonial-container">
                    <div class="testimonial-content">Đây là thẩm mỹ viện tốt nhất bao giờ hết! Dịch vụ thật tuyệt vời và mọi người ở đó rất thân thiện! Đó là một nơi rất sạch sẽ. Các thiết kế cho móng tay và móng chân rất đẹp! Không có nơi nào khác tương tự hoặc tốt hơn. . </div>
                    <div class="testimonial-photo"> <img src="images/it_service/client2.jpg" class="img-responsive" alt="#" width="150" height="150"> </div>
                    <div class="testimonial-meta">
                      <h4>Maria Anderson</h4>
                      <span class="testimonial-position">CFO, Tech NY</span> </div>
                  </div>
                </div>
                <div class="carousel-item">
                  <div class="testimonial-container">
                    <div class="testimonial-content">Đây là thẩm mỹ viện tốt nhất bao giờ hết! Dịch vụ thật tuyệt vời và mọi người ở đó rất thân thiện! Đó là một nơi rất sạch sẽ. Các thiết kế cho móng tay và móng chân rất đẹp! Không có nơi nào khác tương tự hoặc tốt hơn.  </div>
                    <div class="testimonial-photo"> <img src="images/it_service/client3.jpg" class="img-responsive" alt="#" width="150" height="150"> </div>
                    <div class="testimonial-meta">
                      <h4>Maria Anderson</h4>
                      <span class="testimonial-position">CFO, Tech NY</span> </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-5">
          <div class="full"> </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
   <!-- Modal -->
   <div class="modal fade" id="service_bar" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 106%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 about_feature_img padding_right_0">
                    <div class="full text_align_center modal-img"> <img class="img-responsive" src="images/it_service/1.jpg" alt="#" /> </div>
                  </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="full text_align_left">
                    <h3>Kiểu Sơn Móng Hàn Xẻn</h3>
                    <p>Làm móng tay bằng parrafin bao gồm việc nhúng tay vào sáp parafin .</p>
                    <p><strong>Chi phí: </strong>$ 10 đến $ 15 cho mỗi đơn đăng ký.<br />
                      <strong>Thời gian: </strong>45 phút<br />
                      <strong>Thời gian tồn tại: </strong>Khoảng hai đến ba tuần.<br /></p>
                  </div>
                </div>
              
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Model service bar -->

@endsection