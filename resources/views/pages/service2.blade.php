@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="title-holder">
              <div class="title-holder-cell text-left">
                <h1 class="page-title">Dịch vụ</h1>
                <ol class="breadcrumb">
                  <li><a href={{ url('/') }}>Trang chủ</a></li>
                  <li class="active">Dịch vụ</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
  <div class="section padding_layout_2 product_list_main">
    <div class="container">
      <div class="row">
        <div class="col-md-8" style="top: -6px;">
          <div class="row">
            <div class="col-lg-12 pr-3 news_title" style="padding-left: 0">
              <h1 class="pl-3 col-sm-5" itemprop="headline"> Móng Chân</h1>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Mẫu Nail Cho Cô Dâu</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Mẫu Nail Cho Cô Dâu</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/6.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Mẫu Nail Cho Cô Dâu</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
          <div class="side_bar">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                  <span class="input-group-addon">
                  <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                  </span> </div>
              </div>
            </div>
            <div class="side_bar_news">
              <h4>Bài đăng gần đây</h4>
              <div class="recent_post">
                <ul>
                  <li>
                    <p class="post_head"><a href={{ url('chi-tiet') }}>Lí giải vì sao người Việt làm nails nhiều đến như vậy trên đất Mỹ</a></p>
                    <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                  </li>
                  <li>
                    <p class="post_head"><a href={{ url('chi-tiet') }}>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</a></p>
                    <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                  </li>
                  <li>
                    <p class="post_head"><a href={{ url('chi-tiet') }}>Làm móng ở Mỹ: Người lương cao, kẻ nhận thù lao rẻ mạt</a></p>
                    <p class="post_date"><i class="fa fa-calendar" aria-hidden="true"></i> Aug 20, 2017</p>
                  </li>
                </ul>
              </div>
            </div>
            <div class="side_bar_news">
              <h4>Danh mục</h4>
              <div class="categary">
                <ul>
                  <li><a href={{ url('mong-tay') }}><i class="fa fa-caret-right"></i> Móng tay</a></li>
                  <li><a href={{ url('mong-chan') }}><i class="fa fa-caret-right"></i> Móng chân</a></li>
                </ul>
              </div>
            </div>
            <div class="card introduce">
              <div class="card-header">
              </div>
              <div class="card-content collapse show">
                  <div class="card-body">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995&tabs=timeline&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                  </div>
              </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
   <!-- Modal -->
   <div class="modal fade" id="service_bar" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 106%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 about_feature_img padding_right_0">
                    <div class="full text_align_center modal-img"> <img class="img-responsive" src="images/it_service/1.jpg" alt="#" /> </div>
                  </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="full text_align_left">
                    <h3>Kiểu Sơn Móng Hàn Xẻn</h3>
                    <p>Làm móng tay bằng parrafin bao gồm việc nhúng tay vào sáp parafin .</p>
                    <p><strong>Chi phí: </strong>$ 10 đến $ 15 cho mỗi đơn đăng ký.<br />
                      <strong>Thời gian: </strong>45 phút<br />
                      <strong>Thời gian tồn tại: </strong>Khoảng hai đến ba tuần.<br /></p>
                  </div>
                </div>
              
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Model service bar -->
  @endsection