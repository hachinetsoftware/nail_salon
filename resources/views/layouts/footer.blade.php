<!-- footer -->
<footer class="footer_style_2">
    <div class="container-fuild">
      <div class="row">
        <div class="footer_news">
          <div class="row">
            <div class="col-md-3 text_align">
              <div class="main-heading left_text">
                <h2 class="line1">NAILS SALON</h2>
                <P>Cửa hàng chăm sóc móng hàng đầu Việt Nam</P>
              </div>
              <div class="qr"> <img src="images/qr/qrcode.png" alt="#" /> </div>
            </div>
            <div class="col-md-3">
              <div class="main-heading left_text">
                <h2 class="line2"> THÔNG TIN</h2>
              </div>
              <ul class="footer-menu">
                <li><a href={{ url('ve-nails-salon') }}> Về chúng tôi</a></li>
                <li><a href={{ url('lien-he') }}> Liên hệ chúng tôi</a></li>
                <li><a href={{ url('xu-huong') }}>xu</a></li>
                {{-- <li><a href="it_contact.html"><i class="fa fa-angle-right"></i> Phiếu giảm giá</a></li> --}}
              </ul>
            </div>
            <div class="col-md-3">
              <div class="main-heading left_text">
                <h2 class="line3">DỊCH VỤ CỦA CHÚNG TÔI</h2>
              </div>
              <ul class="footer-menu">
                <li><a href={{ url('mong-tay') }}>Dòng nails cao cấp</a></li>
                <li><a href={{ url('mong-tay') }}>Thời trang</a></li>
                <li><a href={{ url('mong-tay') }}>Chăm sóc móng</a></li>
              </ul>
            </div>
            <div class="col-md-3">
              <div class="main-heading left_text">
                <h2 class="line4">Liên hệ</h2>
              </div>
              <p style="line-height: 35px;"><i class="fa fa-map-marker mr-1"></i>87 Nguyễn Thị Minh Khai,TP Vinh
                <br>
                <span><a href="tel:(+84) 24-6290-0388"><i class="fa fa-phone mr-1"></i>(+84) 24-6290-0388</a></span></p>
              <div class="footer_mail-section">
                <form>
                  <fieldset>
                  <div class="field">
                    <input placeholder="Email" type="text">
                    <button class="button_custom"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                  </div>
                  </fieldset>
                </form>
                <ul class="social_icons">
                  <li class="social-icon gp"><span data-toggle="tooltip"  data-placement="bottom" title ="Instagram"><a href="https://www.instagram.com"><i class="fa fa-instagram" aria-hidden="true"></i></a></span></li>
                  <li class="social-icon tw"><span data-toggle="tooltip"  data-placement="bottom" title ="Linedin"><a href="https://www.linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a></span></li>
                  <li class="social-icon tw"><span data-toggle="tooltip"  data-placement="bottom" title ="Twitter"><a href="https://twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></span></li>
                  <li class="social-icon gp"><span data-toggle="tooltip"  data-placement="bottom" title ="Google"><a href="https://plus.google.com/"><i class="fa fa-google-plus" aria-hidden="true"></i></a></span></li>
                  <li class="social-icon fb"><span data-toggle="tooltip"  data-placement="bottom" title ="Facebook"><a href="https://www.facebook.com/"><i class="fa fa-facebook" aria-hidden="true"></i></a></span></li>
                </ul>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <!-- end footer -->