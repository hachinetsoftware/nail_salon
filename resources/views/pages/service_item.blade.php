
  @extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end inner page banner -->
  <!-- section -->
  <div class="section padding_layout_1 gross_layout">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Dòng nails cao cấp</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="row white margin_bottom_30_all">
            <div class="educate_header col-lg-12">
              <div class="left">
              <h3>Móng tay</h3>
              </div>
            </div>
            <div class="col-lg-12 educate_service educate_content">
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/1.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Sơn Móng Hàn Xẻn</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/3.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa xuân</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/4.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Lá Cây</a></h4>
                    </div>
                    <div class="product_price">
                      <p> <span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/5.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/2.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/7.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row white margin_bottom_30_all">
            <div class="educate_header col-lg-12">
              <div class="left">
              <h3>Móng Chân</h3>
              </div>
            </div>
            <div class="col-lg-12 educate_service educate_content">
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/9.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Sơn Móng Hàn Xẻn</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/10.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa xuân</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/11.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Lá Cây</a></h4>
                    </div>
                    <div class="product_price">
                      <p> <span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/12.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price">$25.00</span></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="product_list">
                  <div class="product_img"> <img class="img-responsive" src="images/it_service/13.jpg" alt=""> </div>
                  <div class="product_detail_btm">
                    <div class="center">
                      <h4><a href="#"  data-toggle="modal" data-target="#service_bar">Kiểu Nail Mùa Hè</a></h4>
                    </div>
                    <div class="product_price">
                      <p><span class="new_price"> $12.49</span></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
          <div class="side_bar">
            <div class="side_bar_news white">
              <h4><i class="fa fa-newspaper-o mr-1"></i>Danh mục</h4>
              <div class="categary">
                <ul>
                  <li><a href={{ url('mong-tay') }}><i class="fa fa-caret-right"></i> Dòng nails cao cấp</a></li>
                  <li><a href={{ url('mong-tay') }}><i class="fa fa-caret-right"></i> Thời trang</a></li>
                  <li><a href={{ url('mong-tay') }}><i class="fa fa-caret-right"></i> Chăm sóc móng</a></li>
                </ul>
              </div>
            </div>
            <div class="card introduce">
              <div class="card-header  white">
              </div>
              <div class="card-content collapse show white">
                  <div class="card-body">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=timeline&width=325px&height=500px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
   <!-- Modal -->
   <div class="modal fade" id="service_bar" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 106%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 about_feature_img padding_right_0">
                    <div class="full text_align_center modal-img"> <img class="img-responsive" src="images/it_service/1.jpg" alt="#" /> </div>
                  </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="full text_align_left">
                    <h3>Kiểu Sơn Móng Hàn Xẻn</h3>
                    <p>Làm móng tay bằng parrafin bao gồm việc nhúng tay vào sáp parafin .</p>
                    <p><strong>Chi phí: </strong>$ 10 đến $ 15 cho mỗi đơn đăng ký.<br />
                      <strong>Thời gian: </strong>45 phút<br />
                      <strong>Thời gian tồn tại: </strong>Khoảng hai đến ba tuần.<br /></p>
                  </div>
                </div>
              
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Model service bar -->
  @endsection