@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="side_bar_news">
            <h4>Search</h4>
            <div class="side_bar_search">
              <div class="input-group stylish-input-group">
                <input class="form-control" placeholder="Search" type="text">
              </div>
              <div class="search_bt">
                <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end inner page banner -->
  <!-- section -->
  <div class="section padding_layout_2 news_list">
    <div class="container">
      <div class="row">
        <div class="col-md-8">
          <div class="row white">
            <div class="educate_header col-lg-12">
              <div class="left">
              <h3><i class="fa fa-graduation-cap mr-1"></i>Cho người mới</h3>
              </div>
            </div>
            <div class="col-lg-12 educate_service educate_content">
            <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
              <a href={{ url('dao-tao-chi-tiet') }}>
              <div class="full text_align_left product_list  educate_item">
                <div class="center">
                  <div class="product_img educate_service_img"> <img src="images/it_service/1.jpg" alt="#" /> </div>
                </div>
                <div class="col-md-12">
                    <h4 class="theme_color el-2">Hướng Dẫn Vẽ Nail Aztec Chỉ Trong 5 Phút</h4>
                    <p>
                        <i class="fa fa-calendar"></i> 
                        <span class="ml-1">20/11/2020</span>
                    </p>
                    <p class="el-2">Vẽ nail Aztec chỉ trong 5 phút? Đây không phải là chuyện đùa. Và bạn không cần phải là một chuyên gia vẽ móng cũng có thể thực hiện được. Chỉ cần làm theo các kỹ thuật dưới đây, và thậm chí bạn là một người mới trong ngành nail, bạn cũng có thể tạo ra một mẫu móng Aztec chỉ trong vài phút.</p>
                </div>
              </div>
            </a>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="full text_align_left product_list  educate_item">
                  <div class="center">
                    <div class="product_img educate_service_img"> <img src="images/it_service/2.jpg" alt="#" /> </div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="theme_color el-2">Vẽ Nail Với Cảm Hứng Từ Kỳ Nghỉ Hè Ở Miền Nhiệt Đới</h4>
                      <p>
                          <i class="fa fa-calendar"></i> 
                          <span class="ml-1">20/11/2020</span>
                      </p>
                      <p class="el-2">Mùa hè đang thật sự hiện hữu! Bạn đã có kế hoạch gì cho kỳ nghỉ hè năm nay? Một chuyến du lịch đến các miền nhiệt đới? </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="full text_align_left product_list  educate_item">
                  <div class="center">
                    <div class="product_img educate_service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                      <p>
                          <i class="fa fa-calendar"></i> 
                          <span class="ml-1">20/11/2020</span>
                      </p>
                      <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="full text_align_left product_list  educate_item">
                  <div class="center">
                    <div class="product_img educate_service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                      <p>
                          <i class="fa fa-calendar"></i> 
                          <span class="ml-1">20/11/2020</span>
                      </p>
                      <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="full text_align_left product_list  educate_item">
                  <div class="center">
                    <div class="product_img educate_service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                      <p>
                          <i class="fa fa-calendar"></i> 
                          <span class="ml-1">20/11/2020</span>
                      </p>
                      <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 col-xs-12 margin_bottom_30_all">
                <div class="full text_align_left product_list  educate_item">
                  <div class="center">
                    <div class="product_img educate_service_img"> <img src="images/it_service/3.jpg" alt="#" /> </div>
                  </div>
                  <div class="col-md-12">
                      <h4 class="theme_color el-2">Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                      <p>
                          <i class="fa fa-calendar"></i> 
                          <span class="ml-1">20/11/2020</span>
                      </p>
                      <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn. </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 pull-left">
          <div class="side_bar">
            <div class="side_bar_news white">
              <h4><i class="fa fa-newspaper-o mr-1"></i>Bài đăng gần đây</h4>
              <div class="recent_post">
                <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/1.jpg" alt="#"> </div>
                  <div class="news_feature_cantant flex-column col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Hướng Dẫn Vẽ Nail Aztec Chỉ Trong 5 Phút</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-2">Vẽ nail Aztec chỉ trong 5 phút? Đây không phải là chuyện đùa. Và bạn không cần phải là một chuyên gia vẽ móng cũng có thể thực hiện được. Chỉ cần làm theo các kỹ thuật dưới đây, và thậm chí bạn là một người mới trong ngành nail, bạn cũng có thể tạo ra một mẫu móng Aztec chỉ trong vài phút.  </p>
                  </div>
                </div>
                </div>
                <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/2.jpg" alt="#"> </div>
                  <div class="news_feature_cantant flex-column col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Vẽ Nail Với Cảm Hứng Từ Kỳ Nghỉ Hè Ở Miền Nhiệt Đới</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-2">Mùa hè đang thật sự hiện hữu! Bạn đã có kế hoạch gì cho kỳ nghỉ hè năm nay? Một chuyến du lịch đến các miền nhiệt đới?  </p>
                  </div>
                </div>
                </div>
                <div class="col-lg-12 pr-3 py-3 post_item" style="top: -13px;">
                  <div class="d-flex">
                  <div class="news_feature_img col-sm-5"> <img class="img-responsive" src="images/it_service/3.jpg" alt="#"> </div>
                  <div class="news_feature_cantant flex-column col-sm-7">
                    <a href={{ url('chi-tiet') }}>
                    <h4>Mẫu Nail Hoa Đơn Giản Trên Nền Sơn Ánh Bạc</h4>
                    </a>
                    <span>20/11/2020</span>
                    <p class="el-2">Với những ngày hè nóng như thế này thì việc lựa chọn màu sơn càng nhẹ nhàng càng dễ chịu cho người nhìn.  </p>
                  </div>
                </div>
                </div>
              </div>
            </div>
            <div class="side_bar_news white">
              <h4><i class="fa fa-newspaper-o mr-1"></i>Danh mục</h4>
              <div class="categary">
                <ul>
                  <li><a href={{ url('dao-tao-moi') }}><i class="fa fa-caret-right"></i> Cho người mới</a></li>
                  <li><a href={{ url('dao-tao-moi') }}><i class="fa fa-caret-right"></i> Dạy nails cơ bản</a></li>
                  <li><a href={{ url('dao-tao-moi') }}><i class="fa fa-caret-right"></i> Dạy nails nâng cao</a></li>
                </ul>
              </div>
            </div>
            <div class="card introduce">
              <div class="card-header  white">
              </div>
              <div class="card-content collapse show white">
                  <div class="card-body">
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=timeline&width=325px&height=500px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="500px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>                    
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section -->
   <!-- Modal -->
   <div class="modal fade" id="service_bar" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="width: 106%;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 about_feature_img padding_right_0">
                    <div class="full text_align_center modal-img"> <img class="img-responsive" src="images/it_service/1.jpg" alt="#" /> </div>
                  </div>
                <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="full text_align_left">
                    <h3>Kiểu Sơn Móng Hàn Xẻn</h3>
                    <p>Làm móng tay bằng parrafin bao gồm việc nhúng tay vào sáp parafin .</p>
                    <p><strong>Chi phí: </strong>$ 10 đến $ 15 cho mỗi đơn đăng ký.<br />
                      <strong>Thời gian: </strong>45 phút<br />
                      <strong>Thời gian tồn tại: </strong>Khoảng hai đến ba tuần.<br /></p>
                  </div>
                </div>
              
              </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Model service bar -->
  @endsection