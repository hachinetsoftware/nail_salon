@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
<!-- section -->
<div class="section padding_layout_0">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="full">
          <div class="main_heading text_align_center">
            <h2>Chào mừng đến với Nails Salon</h2>
            <p class="large">Chúng tôi là công ty hàng đầu về chăm sóc móng!</p>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-md-8 col-sm-12 about_cont_news">
        <div class="col-lg-6 col-md-6 col-sm-12 about_feature_img pull-left">
          <div class="full"> <img class="img-responsive" src="images/it_service/post-09.jpg" alt="#" /> 
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 pull-left">
          <div class="full text_align">
            <h1>Naisl Salon</h1>
            <div class="starratin">
              <div class="left"> 
                <i class="fa fa-star-o" aria-hidden="true"></i> 
                <i class="fa fa-star-o" aria-hidden="true"></i> 
                <i class="fa fa-star-o" aria-hidden="true"></i> 
                <i class="fa fa-star-o" aria-hidden="true"></i> 
                <i class="fa fa-star-o" aria-hidden="true"></i> 
                <div class="pull-right">
                  <span><i class="fa fa-thumbs-up" aria-hidden="true"></i> 1 Bình chọn</span>
                  <span><i class="fa fa-user" aria-hidden="true"></i> 1 Người đặt chỗ</span>
                </div>
              </div>
            </div>
            <p>Naisl Salon là salon Nail chuyên nghiệp, sang trọng, nằm trong chuỗi hệ thống Nail lớn nhất tại Việt Nam. Salon mang đến sự đa dạng về dịch vụ Chăm sóc và Trang trí móng, những xu hướng thời trang Nail trên thế giới và đội ngũ nhân viên chuyên nghiệp, ưu tú.</p>
            <div class="about_cont">
              <p><span><a href="tel:(+84) 24-6290-0388"><i class="fa fa-phone"></i> (+84) 24-6290-0388</a></span></p>
              <p><span><i class="fa fa-calendar"></i> Thứ hai - Thứ bảy</span></p>
              <p><span><i class="fa fa-clock-o"></i> 8:30 sáng - 8:30 chiều</span></p>
            </div>
            
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12 about_feature_img padding_right_0">
        <div class="full text_align">
        <h1>Chat đặt chỗ</h1>
        <div class="card-body fb-mess" style="width:100%">
          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=messages&width=370px&height=400px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="100%" height="400px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>        </div>
      </div>
    </div>

  </div>
</div>
<!-- end section -->
<!-- section -->
<div class="section padding_layout_0">
  <div class="container">
    <div class="row">
      <div class="col-md-12 center">
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/3.jpg" alt=""></a>
        </figure>
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/2.jpg" alt=""></a>
        </figure>
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/4.jpg" alt=""></a>
        </figure>
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/5.jpg" alt=""></a>
        </figure>
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/7.jpg" alt=""></a>
        </figure>
        <figure class="col-xs-4 col-sm-2" style="padding:5px; width: 14%">
          <a href="#" data-toggle="modal" data-target="#about_bar"><img class="img-responsive" src="images/it_service/6.jpg" alt=""></a>
        </figure>
        
      </div>
    </div>
  </div>
</div>
<!-- end section -->
<!-- section -->
<div class="section padding_layout_0 star">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhi.ha.1995%2F&tabs=timeline&width=500px&height=500px&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500px" height="500px" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>     
       </div>
      <div class="col-lg-6">
        <div class="form_section voted">
              <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="rating inline-ul" id="votingForm">
                  <li class="m-r-1">Điểm bình chọn: </li>
                  <li class="votePoint" data-point="1"><i class="orange-text fa fa-star-o"></i></li>
                  <li class="votePoint" data-point="2"><i class="orange-text fa fa-star-o"></i></li>
                  <li class="votePoint" data-point="3"><i class="orange-text fa fa-star-o"></i></li>
                  <li class="votePoint" data-point="4"><i class="orange-text fa fa-star-o"></i></li>
                  <li class="votePoint" data-point="5"><i class="orange-text fa fa-star-o"></i></li>
                </ul>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input class="field_custom" placeholder="Họ tên" type="text">
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <textarea class="field_custom" placeholder="Nội dung"></textarea>
              </div>
              <div class="center"><a class="btn main_bt" href="#">Bình chọn</a></div>
        </div>
        <div class="post_commt_form">
          <iframe name="f29195a16aaa28" width="550px" height="100px" data-testid="fb:comments Facebook Social Plugin" title="fb:comments Facebook Social Plugin" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" allow="encrypted-media" src="https://www.facebook.com/v5.0/plugins/comments.php?app_id=&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1fe5789dfa6558%26domain%3Dnetbee.vn%26origin%3Dhttps%253A%252F%252Fnetbee.vn%252Ff2fbf095e80eba8%26relation%3Dparent.parent&amp;container_width=1112&amp;height=100&amp;href=https%3A%2F%2Fnetbee.vn%2Ftin-tuc%2F150%2Fho-tro-cua-chinh-phu-uc-doi-voi-du-hoc-sinh-trong-thoi-diem-dich-covid-19&amp;locale=en_US&amp;numposts=5&amp;sdk=joey&amp;version=v5.0&amp;width=550" style="border: none; visibility: visible; width: 550px; height: 262px;" class=""></iframe>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end section -->
<!-- Modal -->
<div class="modal fade" id="about_bar" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" style="width: 106%;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
      </div>
      <div class="modal-body">
          <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 about_feature_img padding_right_0">
                  <div class="full text_align_center modal-img"> <img class="img-responsive" src="images/it_service/3.jpg" alt="#" /> </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>
<!-- End Moal -->


@endsection