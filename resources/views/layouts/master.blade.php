<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Nails Salon</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- mobile metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="initial-scale=1, maximum-scale=1">
	<!-- site metas -->
	<title>Nails Salon</title>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- site icons -->
	<link rel="icon" href="{{asset('images/fevicon/fevicon.png')}}" type="image/gif">
	<!-- bootstrap css -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<!-- Site css -->
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<!-- responsive css -->
	<link rel="stylesheet" href="{{asset('css/responsive.css')}}">
	<!-- colors css -->
	<link rel="stylesheet" href="{{asset('css/colors1.css')}}">
	<!-- custom css -->
	<link rel="stylesheet" href="{{asset('css/custom.css')}}">
	<!-- wow Animation css -->
	<link rel="stylesheet" href="{{asset('css/animate.css')}}">
	<!-- revolution slider css -->
	<link rel="stylesheet" type="text/css" href="{{asset('revolution/css/settings.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('revolution/css/layers.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('revolution/css/navigation.css')}}">
	<link rel="stylesheet" href="{{asset('vendor/laravel-admin/font-awesome/css/font-awesome.min.css')}}">
	<!-- calendar css -->
	<link rel="stylesheet" href="{{asset('css/jquery-pseudo-ripple.css')}}">
	<link rel="stylesheet" href="{{asset('css/jquery-nao-calendar.css')}}">
</head>
<body id="default_theme" class="it_service">
	<!-- loader -->
	<div class="bg_load"> <img class="loader_animation" src="images/loaders/loader_1.png" alt="#" /> </div>
	<!-- end loader -->
	@include('layouts.header')

	
	@yield('NoiDung')
	

	@include('layouts.footer')
	<!-- js section -->
	<script src="{{asset('js/jquery.min.js')}}"></script>
	<script src="{{asset('js/bootstrap.min.js')}}"></script>
	<!-- menu js -->
	<script src="{{asset('js/menumaker.js')}}"></script>
	<!-- wow animation -->
	<script src="{{asset('js/wow.js')}}"></script>
	<!-- custom js -->
	<script src="{{asset('js/custom.js')}}"></script>
	<!-- revolution js files -->
	<script src="{{asset('revolution/js/jquery.themepunch.tools.min.js')}}"></script>
	<script src="{{asset('revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
	<script src="{{asset('revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
	<!-- calendar js -->
	<script src="{{asset('js/jquery-pseudo-ripple.js')}}"></script>
	<script src="{{asset('js/jquery-nao-calendar.js')}}"></script>
	<script>
		$('.myCalendar').calendar({
		date: new Date(),
		autoSelect: false, // false by default
		select: function(date) {
		console.log('SELECT', date)
		},
		toggle: function(y, m) {
		console.log('TOGGLE', y, m)
		}
		})
	</script>
	<script type="text/javascript">

		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-36251023-1']);
		_gaq.push(['_setDomainName', 'jqueryscript.net']);
		_gaq.push(['_trackPageview']);

		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();

	</script>
	<script>
		new WOW().init();
	 </script>
</body>
</html>