@extends('layouts.master')

@section('NoiDung')
<!-- inner page banner -->
<div id="inner_banner" class="section inner_banner_section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="side_bar_news">
              <h4>Search</h4>
              <div class="side_bar_search">
                <div class="input-group stylish-input-group">
                  <input class="form-control" placeholder="Search" type="text">
                </div>
                <div class="search_bt">
                  <button class="col-md-12 btn btn_search" type="submit" data-toggle="tooltip"  data-placement="top" title ="Tìm kiếm" ><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end inner page banner -->
  <!-- section -->
<div class="section padding_layout_1">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="full">
            <div class="main_heading text_align_center">
              <h2>Đặt lịch</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 about_cont_news">
            <!-- Calender booking -->
            <div class="container-calendar">
                <div class="myCalendar"></div>
            </div>
            <!-- end Calender booking -->
            <form class="form_contant" action={{ url('/') }}>
                <fieldset>
                  <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input class="field_custom" placeholder="Họ tên" type="text">
                  </div>
                  <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input class="field_custom" placeholder="Số điện thoại" type="text">
                  </div>
                  <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input class="field_custom" placeholder="Email" type="email">
                  </div>
                  <div class="field col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea class="field_custom" placeholder="Chi tiết"></textarea>
                  </div>
                  <div class="center col-lg-12 col-md-12 col-sm-12 col-xs-12"><a class="btn main_bt" href="#">Gửi ngay</a></div>
                </fieldset>
              </form>
            </div>
        
        <div class="col-lg-4 col-md-4 col-sm-12 about_feature_img padding_right_0">
          <div class="widget widget_text">
            <h2 class="widget-title" itemprop="name">Giờ làm việc</h2>
            <div class="textwidget">
              <p>Thứ Hai: 9:00 AM &#8211; 7:00 PM</p>
              <p>Thứ Ba: 9:00 AM &#8211; 7:00 PM</p>
              <p>Thứ Tư: 9:00 AM &#8211; 7:00 PM</p>
              <p>Thứ Năm: 9:00 AM &#8211; 7:00 PM</p>
              <p>Thứ Sáu: 9:00 AM &#8211; 7:00 PM</p>
              <p>Thứ Bảy: 9:00 AM &#8211; 7:00 PM</p>
              <p>Chủ Nhật: Đóng Cửa</p>
            </div>
          </div>
        </div>
      </div>
  
    </div>
  </div>
  <!-- end section -->


@endsection